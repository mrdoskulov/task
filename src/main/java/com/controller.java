package com;

import com.model.Files;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by jungle on 5/13/17.
 */
public class controller {
}

@Controller
class MainController{
    @Autowired
    private FilesRepository filerepo;

    @RequestMapping(path="/upload", method = RequestMethod.POST)
    public @ResponseBody
    String upload(@RequestParam("name") String name,
                  @RequestParam("file") MultipartFile file) throws IOException {
        if(file.isEmpty()){
            return "Add file";
        }else{
            byte[] bytes = file.getBytes();

            long size = file.getSize();
            String type = file.getContentType();

            if(!type.equals("text/plain")){
                return "{'status':1, 'detail':'file type must be txt'";
            }
            long nano = System.nanoTime();
            String name2 = nano + ".txt";

            Date today = Calendar.getInstance().getTime();

            File dir = new File("/home/jungle/IdeaProjects/tmp/task/src/main/templates/files/"+today+"/");
            File dir2 = new File("/home/jungle/IdeaProjects/tmp/task/src/main/templates/spam");

            if (!dir.exists()) {
                dir.mkdirs();
            }

            File serverFile = new File(dir.getAbsolutePath()+ File.separator + name2);
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));

            stream.write(bytes);
            stream.close();

            String line = null;
            int types = 0;

            FileReader fileReader = new FileReader(serverFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String name_file = null;
            while((line = bufferedReader.readLine()) != null) {
                name_file += line;
            }

            Boolean is = name_file.contains("spam");
            if(is == Boolean.TRUE){
                types = 1;
                serverFile.delete();
                File names = new File(dir2.getAbsolutePath()+ File.separator + name2);
                dir = dir2;

                BufferedOutputStream stream2 = new BufferedOutputStream(new FileOutputStream(names));

                stream2.write(bytes);
                stream2.close();
            }

            Files pic = new Files();
            pic.setName(name);
            pic.setPath(dir+"/"+name2);
            pic.setType(types);
            filerepo.save(pic);
            }

            return "{'status':2, 'detail':'successfully added new file " + name + "'}";
        }


    @RequestMapping(path="/get_files", method = RequestMethod.GET)
    public @ResponseBody
    List<Files> get_files(){
        return filerepo.findByType(0);
    }

    @RequestMapping(path = "/get_spam", method = RequestMethod.GET)
    public @ResponseBody
    List<Files> get_spam(){
        return filerepo.findByType(1);
    }

    @RequestMapping(path = "/delete_spam/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    String delete_spam(@PathVariable int id){
        Files file = filerepo.findOne(id);
        if(file == null){
            return "No such file";
        }
        File serverFile = new File(file.getPath());
        serverFile.delete();
        filerepo.delete(id);
        return "deleted file";
    }
        }
