package com;

import com.model.Files;
import org.springframework.cglib.core.Predicate;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by abylay on 3/12/17.
 */
public interface FilesRepository extends CrudRepository<Files, Integer> {
    public List<Files> findAllByOrderByIdAsc();
    public List<Files> findByType(int a);
}
